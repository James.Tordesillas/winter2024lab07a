import java.util.Scanner;

public class SimpleWar{
	public static void main(String[] args){
		Deck gamePile = new Deck();
		gamePile.shuffle();
		
		int player1Points = 0;
		int player2Points = 0;
		
		while(gamePile.length() != 0) {
			System.out.println("\n-------------------------------------------------------------------");
			System.out.println("ROUND: " + (26-gamePile.length()/2 + 1) + "\n");
			
			System.out.println("POINTS:");
			System.out.println("Player 1 POINTS: " + player1Points);
			System.out.println("Player 2 POINTS: " + player2Points);
			
			Card player1Card = gamePile.drawTopCard();
			Card player2Card = gamePile.drawTopCard();
			
			double player1Score = player1Card.calculateScore();
			double player2Score = player2Card.calculateScore();
			
			System.out.println("\nDRAWS:");
			System.out.println("Player 1 has " + player1Card);
			System.out.println("Score: " + player1Card.calculateScore());
			
			System.out.println("\nPlayer 2 has " + player2Card);
			System.out.println("Score: " + player2Card.calculateScore());
			
			System.out.println("\n**********************************************");
			if (player1Score > player2Score){
				System.out.println("Player 1 WON THE ROUND!");
				player1Points += 1;
			}
			else {
				System.out.println("Player 2 WON THE ROUND!");
				player2Points += 1;
			}
			System.out.println("**********************************************\n");
			
			System.out.println("Point Changes:");
			System.out.println("Player 1 POINTS: " + player1Points);
			System.out.println("Player 2 POINTS: " + player2Points);
			System.out.println("\n-------------------------------------------------------------------\n");
			
			boolean next = false;
			while (next == false && gamePile.length() != 0){
				System.out.println("Type 'n' then ENTER to draw again");
				String input = new Scanner(System.in).nextLine();
				
				if (input.equals("n")){
					next = true;
				}
				else{
					System.out.println("Invalid Input");
				}
			}
		}
		
		if (player1Points > player2Points){
			System.out.println("\nPLAYER 1 WINS!!");
		}
		else if (player1Points < player2Points) {
			System.out.println("\nPLAYER 2 WINS!!");
		}
		else {
			System.out.println("\nIT'S A TIE!!");
		}
	}
}